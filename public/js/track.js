window.addEventListener('message', function(message) {
  if (data = JSON.parse(message.data)) {
    switch (data.message) {
      case 'sync':
        $.ajax({
          method: "POST",
          url: "/data/track",
          data: data.context,
          dataType: "json"
        });
        break;
      case 'evaluate':
        $.ajax({
          method: "POST",
          url: "/data/showEvaluation",
          data: data.context,
          dataType: "json",
          success: function(result) {
            if (result.evaluation.showUpsell > 50) {
              $.ajax({
                method: "POST",
                url: "/data/recommendEvaluation",
                data: data.context,
                dataType: "json",
                success: function(result) {
                  parent.postMessage(JSON.stringify({
                    message: 'recommendation',
                    result: result
                  }), '*');
                }
              });
            }
          }
        });
        break;
    }
  }
});
