"use strict";

window.FuzzyAi = window.FuzzyAi === undefined ? {} : FuzzyAi;

// Grab the current state of the cart
var xhr = new XMLHttpRequest;
xhr.open('GET', '/cart.js', true);
xhr.onreadystatechange = function() {
  var cart;
  if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
    if (cart = JSON.parse(xhr.responseText)) {
      FuzzyAi.customer = FuzzyAi.customer === undefined ? {} : FuzzyAi.customer;
      FuzzyAi.customer.cart = cart;
    }
  }
}
xhr.send();

// Set up iFrame
var ifrmSrc = FuzzyAi.ApiRoot + '/embed?shop=' + Shopify.shop;
var ifrm = document.querySelector("iframe[src='" + ifrmSrc + "']");

if (!ifrm) {
  ifrm = document.createElement('iframe');
  ifrm.id = 'fuzzyai-channel';
  ifrm.style.display = 'none';
  document.body.insertBefore(ifrm, document.body.children[0]);
  ifrm.setAttribute('src', ifrmSrc);
}

window.addEventListener('message', function(message) {
  var data;
  if (data = JSON.parse(message.data)) {
    if (data.message == 'recommendation') {
      var product = data.result.product;
      if (product) {
        $('#fuzzyai-upsell .title').html(product.title);
        $('#fuzzyai-upsell .name').html(product.title);
        $('#fuzzyai-upsell .price').html(product.variants[0].price);
        $('#fuzzyai-upsell .image img').attr('src', product.images[0].src);
        $('#fuzzyai-upsell .description').html(product.body_html);
        $('#fuzzyai-upsell button').attr('data-id', product.variants[0].id);
        $('#fuzzyai-upsell .close').click(function(e) {
          $('#fuzzyai-upsell').hide();
        });
        $('#fuzzyai-upsell button').click(function(e) {
          e.preventDefault();
          $.ajax({
            method: "POST",
            url: "/cart/add.js",
            data: {quantity: 1, id: product.variants[0].id},
            dataType: "json",
            success: function(result) {
              $('#fuzzyai-upsell').hide();
              location.reload();
            }
          });
        })
        $('#fuzzyai-upsell').show();
      }
    }
  }
});

ifrm.addEventListener('load', function(data) {
  ifrm.contentWindow.postMessage(JSON.stringify({
    message: 'sync',
    context: FuzzyAi
  }), '*');

  if (FuzzyAi.inCart) {
    // see if we need to make a Recommendation
    ifrm.contentWindow.postMessage(JSON.stringify({
      message: 'evaluate',
      context: FuzzyAi
    }), '*')
  }
});
