# shop.coffee
# Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

db = require 'databank'

Shop = db.DatabankObject.subClass('shop')

Shop.schema =
  pkey: 'url'
  fields: [
    'url',
    'access_token',
    'api_key'
    'theme_id'
    'showAgentID',
    'recommendAgentID',
    'createdAt',
    'updatedAt'
  ],
  indices: ['shop']

module.exports = Shop
