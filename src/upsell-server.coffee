# upsell-server.coffee
# Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>

_ = require 'lodash'
assert = require 'assert'
crypto = require 'crypto'
APIClient = require 'fuzzy.ai'
Microservice = require 'fuzzy.ai-microservice'
path = require 'path'
async = require 'async'
bodyParser = require 'body-parser'
express = require 'express'
session = require 'express-session'
{Databank, DatabankObject} = require 'databank'
DatabankStore = require('connect-databank')(session)
pug = require 'pug'

{ showAgent, recommendAgent } = require './agents'
Customer = require './customer'
HTTPError = require './httperror'
Shop = require './shop'
version = require './version'
web = require 'fuzzy.ai-web'

class UpsellServer extends Microservice

  getName: ->
    "upsell-server"

  environmentToConfig: (env) ->
    config = super env
    _.extend config,
      apiServer: env['API_SERVER'] || 'https://api.fuzzy.ai'
      apiKey: 'default-key-do-not-use'
      appUrl: env['APP_URL']
      shopifyKey: env['SHOPIFY_KEY']
      shopifySecret: env['SHOPIFY_SECRET']
    config

  setupMiddleware: (exp) =>
    exp.apiClient = new APIClient
      root: @config.apiServer
      key: @config.apiKey
    exp.shopify = new web.WebClient

    exp.use bodyParser.urlencoded({extended: true})
    exp.use bodyParser.json()
    exp.set('view engine', 'pug')
    exp.use express.static path.join(__dirname, '..', 'public')

    # Use a separate connection for sessions

    db = Databank.get @config.driver, @config.params

    db.connect @config.params, (err) ->
      if err
        console.error "connection error"

    store = new DatabankStore db, exp.log

    exp.use session
      secret: 's3kr3t'
      store: store
      resave: true
      saveUninitialized: true

  # Make sure Customer and Shop always use current @db value

  startDatabase: (callback) ->
    super (err) =>
      if err?
        callback err
      else
        Customer.bank = Shop.bank = =>
          assert _.isObject(@db)
          @db

        callback null

  setupRoutes: (exp) ->
    exp.get '/shopify/install', @_shopifyInstall
    exp.get '/shopify/auth', @_shopifyAuth
    exp.post '/shopify/webhook/uninstalled', @_shopifyUninstalled

    exp.get '/settings', @_getSettings
    exp.post '/settings', @_postSettings
    exp.get '/embed', @_getEmbed

    exp.post '/data/track', @_trackView
    exp.post '/data/showEvaluation', @_showEvaluation
    exp.post '/data/recommendEvaluation', @_recommendEvaluation

    exp.get '/', @dontLog, (req, res, next) ->
      res.json
        status: 'OK'

  getSchema: ->
    schema =
      'customer': Customer.schema
      'shop': Shop.schema

    schema

  _shopifyInstall: (req, res, next) ->
    config = req.app.config
    shop_url = req.query.shop
    scopes = "read_orders,read_products,write_script_tags,read_themes,write_themes"

    install_url = "https://#{shop_url}/admin/oauth/authorize?\
      client_id=#{config.shopifyKey}&scope=#{scopes}&\
      redirect_uri=#{config.appUrl}/shopify/auth"

    res.redirect install_url

  _shopifyAuth: (req, res, next) ->
    config = req.app.config
    shopify = req.app.shopify
    shop_url = req.query.shop

    h = _.omit req.query, ['hmac', 'signature']
    query = _.keys(h).sort().map((key) -> "#{key}=#{h[key]}").join('&')

    hmac = crypto.createHmac('sha256', config.shopifySecret)
    hmac.update(query)
    digest = hmac.digest('hex')

    if req.query.hmac != digest
      next new HTTPError("Authentication Failed.", 403)

    # Do all of the required setup steps
    async.waterfall [
      (callback) ->
        # First: See if we already have a record for this shop
        _ensureSession req, shop_url, callback
      (shop, callback) ->
        # Setup an uninstall hook.
        url = "https://#{shop_url}/admin/script_tags/count.json"
        headers =
          'Content-Type': 'application/json; charset=utf-8'
          'X-Shopify-Access-Token': shop.access_token
        shopify.get url, headers, (err, junk, body) ->
          if err
            callback err
          else
            json = JSON.parse body
            if json['count'] == 0
              # Add webhook for uninstall
              url = "https://#{shop_url}/admin/webhooks.json"
              payload =
                webhook:
                  topic: "app\/uninstalled"
                  address: "#{config.appUrl}\/shopify\/webhook\/uninstalled"
                  format: "json"
              headers =
                'Content-Type': 'application/json; charset=utf-8'
                'X-Shopify-Access-Token': shop.access_token
              shopify.post url, headers, JSON.stringify(payload), (err, junk, body) ->
                if err
                  callback err
                else
                  callback null, shop
            else
              callback null, shop
      (shop, callback) ->
        # Check if we have added our script tag already
        url = "https://#{shop_url}/admin/script_tags/count.json"
        headers =
          'Content-Type': 'application/json; charset=utf-8'
          'X-Shopify-Access-Token': shop.access_token
        shopify.get url, headers, (err, junk, body) ->
          if err
            callback err
          else
            json = JSON.parse body
            if json['count'] == 0
              # Add our upsell.js script tag.
              url = "https://#{shop_url}/admin/script_tags.json"
              payload =
                script_tag:
                  event: "onload"
                  src: "#{config.appUrl}\/js\/upsell.js"
              headers =
                'Content-Type': 'application/json; charset=utf-8'
                'X-Shopify-Access-Token': shop.access_token
              shopify.post url, headers, JSON.stringify(payload), (err, junk, body) ->
                if err
                  callback err
                else
                  callback null, shop
            else
              callback null, shop
      (shop, callback) ->
        # Find the current theme
        url = "https://#{shop_url}/admin/themes.json"
        headers =
          'Content-Type': 'application/json; charset=utf-8'
          'X-Shopify-Access-Token': shop.access_token
        shopify.get url, headers, (err, junk, body) ->
          if err
            callback err
          else
            json = JSON.parse body
            theme = _.find(json['themes'], {'role': 'main'})
            shop.theme_id = theme.id
            shop.save (err, shop) ->
              if err
                callback err
              else
                callback null, shop
      (shop, callback) ->
        # add our snippets
        url = "https://#{shop_url}/admin/themes/#{shop.theme_id}/assets.json"
        async.each ['snippets/fuzzyai-head.liquid', 'snippets/fuzzyai-cart.liquid'], (key, callback) ->
          t = pug.compileFile("./src/#{key}.pug")
          payload =
            asset:
              key: key
              value: t({appUrl: config.appUrl})
          headers =
            'Content-Type': 'application/json; charset=utf-8'
            'X-Shopify-Access-Token': shop.access_token
          shopify.put url, headers, JSON.stringify(payload), (err, res, body) ->
            if err
              callback err
            else
              callback null
        , (err) ->
          if err
            callback err
          else
            callback null, shop
      (shop, callback) ->
        # update base template to add header snippet
        url = "https://#{shop_url}/admin/themes/#{shop.theme_id}/assets.json?asset[key]=layout/theme.liquid"
        headers =
          'Content-Type': 'application/json; charset=utf-8'
          'X-Shopify-Access-Token': shop.access_token
        shopify.get url, headers, (err, res, body) ->
          json = JSON.parse body
          html = json['asset']['value']
          if not _.includes html, "{% include 'fuzzyai-head' %}"
            parts = _.split html, '<head>', 2
            html = parts[0] + "<head>{% include 'fuzzyai-head' %}" + parts[1]
            url = "https://#{shop_url}/admin/themes/#{shop.theme_id}/assets.json"
            payload =
              asset:
                key: 'layout/theme.liquid'
                value: html
            headers =
              'Content-Type': 'application/json; charset=utf-8'
              'X-Shopify-Access-Token': shop.access_token
            shopify.put url, headers, JSON.stringify(payload), (err, res, body) ->
              if err
                callback err
              else
                callback null, shop
          else
            callback null, shop
      (shop, callback) ->
        url = "https://#{shop_url}/admin/themes/#{shop.theme_id}/assets.json?asset[key]=templates/cart.liquid"
        headers =
          'Content-Type': 'application/json; charset=utf-8'
          'X-Shopify-Access-Token': shop.access_token
        shopify.get url, headers, (err, res, body) ->
          json = JSON.parse body
          html = json['asset']['value']
          if not _.includes html, "{% include 'fuzzyai-cart' %}"
            html += "{% include 'fuzzyai-cart' %}"
            url = "https://#{shop_url}/admin/themes/#{shop.theme_id}/assets.json"
            payload =
              asset:
                key: 'templates/cart.liquid'
                value: html
            headers =
              'Content-Type': 'application/json; charset=utf-8'
              'X-Shopify-Access-Token': shop.access_token
            shopify.put url, headers, JSON.stringify(payload), (err, res, body) ->
              if err
                callback err
              else
                callback null, shop
          else
            callback null, shop

    ], (err, shop) ->
      if err
        next err
      else
        res.redirect "/settings"

  _shopifyUninstalled: (req, res, next) ->
    config = req.app.config

    # verify the webhook signature
    hmac = req.headers['x-shopify-hmac-sha256']

    message = JSON.stringify req.body
    message = message.split('/').join('\\/')
    message = message.split('&').join('\\u0026')

    digest = crypto.createHmac('SHA256', config.shopifySecret).update(message).digest('base64')

    if hmac == digest
      shop_url = req.headers['x-shopify-shop-domain']
      Shop.get shop_url, (err, shop) ->
        if err
          next err
        else
          shop.del (err) ->
            if err
              next err
            else
              res.json
                status: 'OK'
    else
      res.json
        status: 'OK'


  _getSettings: (req, res, next) ->
    res.render 'settings',
      apiKey: req.app.config.shopifyKey,
      shop: req.session?.shop

  _postSettings: (req, res, next) ->
    client = req.app.apiClient
    shop = new Shop(req.session.shop)
    shop.api_key = req.body.fuzzyKey
    client.key = shop.api_key

    async.waterfall [
      (callback) ->
        if not shop.showAgentID
          client.newAgent showAgent, (err, result) ->
            if err
              callback err
            else
              shop.showAgentID = result.id
              callback null
        else
          callback null
      (callback) ->
        if not shop.recommendAgentID
          client.newAgent recommendAgent, (err, result) ->
            if err
              callback err
            else
              shop.recommendAgentID = result.id
              callback null
        else
          callback null
      (callback) ->
        shop.save callback
      ], (err, shop) ->
        req.session.shop = shop
        res.json
          shop: shop

  _getEmbed: (req, res, next) ->
    shop_url = req.query.shop
    config = @config

    _ensureSession req, shop_url, (err, shop) ->
      res.render 'embed',
        apiKey: req.app.config.shopifyKey,
        shop: shop

  _trackView: (req, res, next) ->
    props = req.body
    # We are viewing a pro
    _getOrCreateCustomer req.session.id, (err, customer) ->
      if err
        next err
      else
        customer.lastSeen = Date.now()
        if props.customer
          customer.shopify_id = props.customer.id
          customer.cart = props.customer.cart
        if props.product
          # We are viewing a product
          if not customer.products
            customer.products = {}
          if customer.products[props.product.id]
            customer.products[props.product.id].views++
          else
            customer.products[props.product.id] = views: 1
          customer.products[props.product.id].lastSeen = Date.now()
        customer.save (err, customer) ->
          if err
            next err
          else
            res.json
              status: "OK"
              customer: customer

  _showEvaluation: (req, res, next) ->
    shopify = req.app.shopify
    shop = new Shop(req.session.shop)
    client = req.app.apiClient
    client.key = shop.api_key
    inputs =
      userSessionCount: 0
      productsViewedCount: 0
      previousPurchaseCount: 0
      newProductsSinceLastPurchase: 0
    async.waterfall [
      (callback) ->
        _syncProducts shopify, shop, callback
      (result, callback) ->
        shop = result
        Customer.get req.session.id, callback
      (customer, callback) ->
        _syncOrders shopify, shop, customer, callback
      (customer, callback) ->
        if customer.shopify_id
          inputs.userSessionCount = 1
        inputs.productsViewedCount = _.size customer.products
        inputs.previousPurchaseCount = _.size customer.orders
        lastOrder = _.maxBy customer.orders, 'updated_at'
        if lastOrder
          inputs.newProductsSinceLastPurchase = _.size(_.filter(shop.products, (p) ->
            p.update_at > lastOrder.updated_at
          ))

        client.evaluate shop.showAgentID, inputs, (err, evaluation) ->
          if err
            callback err
          else
            callback null, evaluation
    ], (err, evaluation) ->
      if err
        next err
      else
        res.json
          status: 'OK'
          evaluation: evaluation

  _recommendEvaluation: (req, res, next) ->
    shopify = req.app.shopify
    shop = new Shop(req.session.shop)
    client = req.app.apiClient
    client.key = shop.api_key
    inputs = []
    products = []

    async.waterfall [
      (callback) ->
        _syncProducts shopify, shop, callback
      (result, callback) ->
        shop = result
        Customer.get req.session.id, callback
      (customer, callback) ->
        _syncOrders shopify, shop, customer, callback
      (customer, callback) ->
        lastOrder = _.maxBy customer.orders, 'updated_at'
        products = _.filter shop.products, (p) ->
          not _.size(_.filter(customer.cart.items, {product_id: "#{p.id}"}))
        _.forEach products, (product) ->
          input =
            didUserView: _.toInteger(_.has customer.products, product.id)
            previousPurchaseCount: _.size(_.filter(customer.orders, (o) ->
                _.find(o.line_items, {product_id: product.id})
              ))
            isCurrentlyInCart: _.size(_.filter(customer.cart.items, {product_id: "#{product.id}"}))
            isProductNewSinceLastPurchase: _.toInteger(lastOrder?.updated_at < product.updated_at)
          inputs.push input
        client.evaluate shop.recommendAgentID, inputs, (err, evaluation) ->
          if err
            callback err
          else
            callback null, evaluation

    ], (err, evaluation) ->
      if err
        next err
      else
        max = _.maxBy evaluation, 'affinity'
        id = _.findIndex evaluation, max

        res.json
          status: 'OK'
          evaluation: evaluation
          product: products[id]

module.exports = UpsellServer

_ensureSession =  (req, shop_url, callback) ->
  config = req.app.config
  shopify = req.app.shopify
  code = req.query.code

  Shop.get shop_url, (err, shop) ->
    if not err
      req.session.shop = shop
      callback null, shop
    else
      if err.name != 'NoSuchThingError'
        callback err
      else
        url = "https://#{shop_url}/admin/oauth/access_token"
        payload =
          client_id: config.shopifyKey
          client_secret: config.shopifySecret
          code: code
        headers =
          'Content-Type': 'application/json; charset=utf-8'

        shopify.post url, headers, JSON.stringify(payload), (err, _, body) ->
          if err
            next err
          else
            json = JSON.parse body
            data =
              url: shop_url
              access_token: json['access_token']
              api_key: ''
            Shop.create data, (err, shop) ->
              if err
                callback err
              else
                req.session.shop = shop
                callback null, shop

_getOrCreateCustomer = (session_id, callback) ->
  Customer.get session_id, (err, customer) ->
    if not err
      callback null, customer
    else
      if err.name != 'NoSuchThingError'
        callback err
      else
        props =
          session_id: session_id
        Customer.create props, (err, customer) ->
          if err
            callback err
          else
            callback null, customer

_syncOrders = (shopify, shop, customer, callback) ->
  if customer.shopify_id
    url = "https://#{shop.url}/admin/customers/#{customer.shopify_id}/orders.json"
    headers =
      'Content-Type': 'application/json; charset=utf-8'
      'X-Shopify-Access-Token': shop.access_token
    shopify.get url, headers, (err, junk, body) ->
      if err
        callback err
      else
        json = JSON.parse body
        customer.orders = json.orders
        customer.save (err, customer) ->
          if err
            callback err
          else
            callback null, customer
  else
    callback null, customer

_syncProducts = (shopify, shop, callback) ->
  latest = _.maxBy shop.products, (o) ->
    o.updated_at
  url = "https://#{shop.url}/admin/products.json?published_status=published"
  if latest
    url += "&updated_at_min=#{latest.updated_at}"
  headers =
    'Content-Type': 'application/json; charset=utf-8'
    'X-Shopify-Access-Token': shop.access_token
  shopify.get url, headers, (err, junk, body) ->
    if err
      callback err
    else
      json = JSON.parse body
      if not shop.products
        shop.products = []
      shop.products = _.uniqBy(_.concat(shop.products, json.products), 'id')
      shop.save (err, shop) ->
        if err
          callback err
        else
          callback null, shop
