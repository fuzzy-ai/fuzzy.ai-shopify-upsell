# customer.coffee
# Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

db = require 'databank'

Customer = db.DatabankObject.subClass('customer')

Customer.schema =
  pkey: 'session_id'
  fields: [
    'session_id',
    'lastSeen',
    'productsViewed',
    'createdAt',
    'updatedAt'
  ],
  indices: ['shop']

module.exports = Customer
