# agents.coffee
# Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>

module.exports.showAgent =
  name: "Show Upsell"
  inputs:
    userSessionCount:
      none: [0, 0.5]
      some: [0.5, 1]
    productsViewedCount:
      veryLow: [0, 1]
      low: [0, 1, 4]
      medium: [1, 4, 7]
      high: [4, 7, 10]
      veryHigh: [7, 10]
    previousPurchaseCount:
      none: [0, 0.5]
      some: [0.5, 1]
    newProductsSinceLastPurchase:
      none: [0, 0.5]
      some: [0.5, 1]
    compProductsNotInCart:
      none: [0, 0.5]
      some: [0.5, 1]
  outputs:
    showUpsell:
      veryLow: [0, 25]
      low: [0, 25, 50]
      medium: [25, 50, 75]
      high: [50, 75, 100]
      veryHigh: [75, 100]
  rules: [
    "IF userSessionCount IS none THEN showUpsell IS veryHigh WITH 1.0"
    "IF userSessionCount IS some AND previousPurchaseCount is none THEN showUpsell IS veryLow WITH 1.0"
    "productsViewedCount DECREASES showUpsell WITH 0.5"
    "IF compProductsNotInCart IS none THEN showUpsell IS veryLow WITH 1.0"
    "IF compProductsNotInCart IS some THEN showUpsell IS veryHigh WITH 1.0"
    "IF newProductsSinceLastPurchase IS none THEN showUpsell IS veryLow WITH 1.0"
    "IF newProductsSinceLastPurchase IS some THEN showUpsell IS veryHigh WITH 1.0"
  ]


module.exports.recommendAgent =
  name: "Upsell Product Recommendation"
  inputs:
    isComplementary:
      false: [0, 0.5]
      true: [0.5, 1]
    didUserView:
      false: [0, 0.5]
      true: [0.5, 1]
    previousPurchaseCount:
      false: [0, 0.5]
      true: [0.5, 1]
    isCurrentlyInCart:
      false: [0, 0.5]
      true: [0.5, 1]
    isProductNewSinceLastPurchase:
      false: [0, 0.5]
      true: [0.5, 1]
  outputs:
    affinity:
      veryLow: [0, 25]
      low: [0, 25, 50]
      medium: [25, 50, 75]
      high: [50, 75, 100]
      veryHigh: [75, 100]
  rules: [
    "IF isComplementary IS true AND isCurrentlyInCart is false THEN affinity IS veryHigh WITH 1.0"
    "IF isComplementary IS false THEN affinity is low with 1.0"
    "IF isCurrentlyInCart IS true THEN affinity IS veryLow WITH 1.0"
    "IF didUserView IS true THEN affinity IS veryLow WITH 0.5"
    "IF previousPurchaseCount IS true and isCurrentlyInCart is false THEN affinity IS veryHigh WITH 1.0"
    "IF isProductNewSinceLastPurchase IS true THEN affinity IS veryHigh WITH 0.5"
  ]
